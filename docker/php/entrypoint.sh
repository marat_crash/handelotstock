#!/usr/bin/env bash

composer install --no-interaction --no-ansi --optimize-autoloader --apcu-autoloader
rm -rf ~/.composer/cache/*
bin/console server:run 0.0.0.0:8000
