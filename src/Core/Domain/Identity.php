<?php

declare(strict_types=1);

namespace App\Core\Domain;

abstract class Identity
{
    /** @var mixed */
    private $rawValue;

    public function __construct($rawValue)
    {
        if (empty($rawValue)) {
            throw new IdentityNotCreated();
        }

        $this->assertRawValue($rawValue);

        $this->rawValue = $rawValue;
    }

    abstract protected function assertRawValue($rawValue): void ;

    abstract public function equals($another): bool;

    final public function getRawValue()
    {
        return $this->rawValue;
    }
}