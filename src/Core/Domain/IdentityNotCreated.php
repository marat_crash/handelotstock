<?php
declare(strict_types=1);

namespace App\Core\Domain;

final class IdentityNotCreated extends \RuntimeException
{

}