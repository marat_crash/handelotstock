<?php

declare(strict_types=1);

namespace App\Core\Domain;

abstract class StringIdentity extends Identity
{
    final public function __construct(string $rawValue)
    {
        parent::__construct($rawValue);
    }

    public function equals($another): bool
    {
        return
            $another instanceof static
            && static::class === get_class($another)
            && $this->getRawValue() === $another->getRawValue();
    }

    final protected function assertRawValue($rawValue): void
    {
    }
}