<?php

declare(strict_types=1);

namespace App\Core\Domain;

use Ramsey\Uuid\Uuid;

abstract class StringUuidIdentity extends Identity
{
    final public function __construct(string $rawValue)
    {
        parent::__construct($rawValue);
    }

    final public static function generate(): self
    {
        return new static(Uuid::uuid4()->toString());
    }

    public function equals($another): bool
    {
        return
            $another instanceof static
            && static::class === get_class($another)
            && $this->getRawValue() === $another->getRawValue();
    }

    final protected function assertRawValue($rawValue): void
    {
    }
}