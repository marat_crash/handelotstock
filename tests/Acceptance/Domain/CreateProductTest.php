<?php
declare(strict_types=1);

namespace App\Tests\Acceptance\Domain;

use App\Acceptance\Domain\Product;
use App\Acceptance\Domain\ProductId;
use PHPUnit\Framework\TestCase;

class CreateProductTest extends TestCase
{
    public function testSuccess()
    {
        $product = new Product(
            $productId = new ProductId(
                $id = 'some whosaler\'s product ID'
            ),
            $name = 'Apple Iphone 16Gb Original',
            $price = new Money(
                $priceValue = '989.89',
                $currency = Money::EUR
            ),
            $description = 'Some short description of the best product in the World'
        );

        $this->assertEquals($productId, $product->getProductId());
        $this->assertEquals($name, $product->getName());
        $this->assertEquals($price, $product->getPrice());
        $this->assertEquals($description, $product->getDescription());
        $this->assertNotNull($price);
        $this->assertNotNull($productId);
        $this->assertNotEmpty($name);
    }
}